//unshift method
function reverseArray(oldArray){
    var reversedArray = [];

    for(var i = 0; i < oldArray.length; i++){
        reversedArray.unshift(oldArray[i])
    }

    return reversedArray;
}

//push/pop method
function reverseArray(oldArray){
    var reversedArray = [];

    for(var i = oldArray.length; i > 0; i--){
        reversedArray.push(oldArray.pop());
    }

    return reversedArray;
}

function reserveArrayInPlace(){

}

console.log(reverseArray(["A", "B", "C"]));