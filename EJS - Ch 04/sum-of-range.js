function range(start, end, step){
    if(step == null){
        step = 1;
    }

    var result = [];
    if(step > 0) {
        for (var i = start; i <= end; i += step) {
            result.push(i);
        }
    } else {
            for(var i = start; i >= end; i += step){
                result.push(i);}
    }
    return result;
}

//using reduce method
function sum(numbers){
    var total = numbers.reduce(function(previous, current) {
        return previous + current;
    });
    return total;

}

//using for loop
function sum1(numbers){
    var args = [];
    for(var i = 0; i < numbers.length; i++){
        args.push(numbers[i]);
    }

    var total = args.reduce(function(previous, current) {
        return previous + current;
    });
    return total;

}

console.log(range(1, 10));
console.log(sum(range(1,10)));
console.log(range(5, 2, -1));