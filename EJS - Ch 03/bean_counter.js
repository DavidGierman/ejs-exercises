//For loop version
function countBs(word, letter){
    var count = 0;
    for(var i = 0; i < word.length; i++){
        if(word.charAt(i) == letter){
            count++;
        }
    }
    return count;
}

console.log(countBs("BBC","B"));

//Recursive inner function version

function countChar(word, letter){
    var count = 0;
    var length = word.length;

    function findChar(length){
        if(word.charAt(length - 1) == letter){
            count++;
        }else if(length == 0){
            return count;
        }
        findChar(length - 1);
    }

    findChar(length);

    return count;

}
console.log(countChar("bbc", "b"));

console.log(countChar("David", "d"));
